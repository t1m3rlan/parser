import com.univocity.parsers.csv.CsvWriter;
import com.univocity.parsers.csv.CsvWriterSettings;
import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Read_and_parceDuplictes {



    static Map<String, List<Integer>> indexes = new HashMap<>();                 //хешмапа для артикулов дочерняя. Ключем берет "артикул", значения - массив с номерами рядов из XLS+1)
    static Map<Integer, Integer> articlesAndRowsInXLS = new HashMap<>();        // хешмапа которая помогает соединять дубликаты номеров рядов XLS
    static HSSFRow currentRow;
    HSSFRow row;
    File fileToRead = FileUtils.getFile("sks.xls");
    File newCSV_File = new File("SKS_new.csv");

    public int rows;
    List<String> itemName = new ArrayList<>();                                    //хранит "наименования" из XLS
    String currentProductGroup = "";
    private ArrayList<String> sortedKeys;                                       //лист для хранения остортированных по возрастанию "ключей" Map indexes
    HSSFSheet sheet;
    public int currentRowNum;

    String[] headersCSV = new String[19];
    CsvWriter dataWriter;
    String[] expectedHeaders = headersCSV;
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
    LocalDate localDate = LocalDate.now();
    ArrayList<Integer> rowsWithData = new ArrayList<>();


    //настройки записи в CSV
    public CsvWriterSettings settings() {
        CsvWriterSettings settings = new CsvWriterSettings();
        settings.setQuoteAllFields(false);
        settings.getFormat().setDelimiter("\t");
        settings.setIgnoreLeadingWhitespaces(false);
        settings.setIgnoreTrailingWhitespaces(false);
        settings.setHeaders(expectedHeaders);
        return settings;
    }

    //открытие XLS с укзанием нужного листа и конечного ряда
    public void Sks_parcer() throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook(FileUtils.openInputStream(fileToRead));
        // get 1th sheet data
        sheet = workbook.getSheetAt(1);
        // get number of rows from sheet
        rows = sheet.getPhysicalNumberOfRows();
        CreateNamesList();
    }
    //извлечение группы товарор (аналог "!группа"  при импорте на сайт)
    public String productGroup() {
        currentRow = sheet.getRow(currentRowNum);
        String productGroup;
        DataFormatter productNameFormatter = new DataFormatter();
        productGroup = productNameFormatter.formatCellValue(currentRow.getCell(3));
        return productGroup;
    }
    //создание списка из всех артикулов
    public void CreateNamesList() {
        String formattedData;
        HSSFDataFormatter dataFormatter = new HSSFDataFormatter();

        for (int r = 8; r < rows; r++) {
            row = this.sheet.getRow(r);
            formattedData = dataFormatter.formatCellValue(row.getCell(0));      //корректное извлечение Артикула из XLS (STRING)
                if (formattedData.equalsIgnoreCase("код")) {              //присваивание "группы" для всех последующих товаров
                    currentRowNum = r;
                    currentProductGroup = productGroup();
                } else if (formattedData.matches("[0-9]+")) {
                    row.getCell(11, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).setCellValue(r);     //запись номера ряда для дальнейшего извлечения
                    //System.out.println(row.getCell(11));
                    row.getCell(12, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).setCellValue(currentProductGroup); //запись Группы товара для дальнейшего извлечения
                    //System.out.println(row.getCell(12));
                    rowsWithData.add(r);
                    itemName.add(formattedData);

                    //  articlesAndSubgroups
                    for (int y = 0; y < rowsWithData.size(); y++) {                         //созадет мапу (порядковый номер; номер артикула)
                        articlesAndRowsInXLS.put(y, rowsWithData.get(y));
                    }
                }
            System.out.println("Reading arcticle \"" + (r-8) + "\" from " + (rows) );
        }

        CreatingMapOfDuplicates(itemName);
        //putHeadersToCSV();
    }

    public void CreatingMapOfDuplicates(List itemName) {

        for (int i = 0; i < itemName.size(); i++) {
            indexes.computeIfAbsent((String) itemName.get(i), c -> new ArrayList<>()).add(articlesAndRowsInXLS.get(i));
        }
        sortByKey();
    }
    //сортировка по возрастанию артикулов
    public void sortByKey() {
        sortedKeys = new ArrayList<>(indexes.keySet());
        Collections.sort(sortedKeys);
        putHeadersToCSV();
    }
    //заголовки столбцов CSV
    public void putHeadersToCSV() {
        headersCSV[0] = "Код артикула";
        headersCSV[1] = "Группа SKS";
        headersCSV[2] = "Подгруппа SKS";
        headersCSV[3] = "Производитель SKS";
        headersCSV[4] = "Цена";
        headersCSV[5] = "Закупочная цена";
        headersCSV[6] = "Валюта";
        headersCSV[7] = "Изображения товаров ";
        headersCSV[8] = "Изображения товаров ";
        headersCSV[9] = "Изображения товаров ";
        headersCSV[10] = "Изображения товаров ";
        headersCSV[11] = "Изображения товаров ";
        headersCSV[12] = "Изображения товаров ";
        headersCSV[13] = "Наличие SKS";
        headersCSV[14] = "Наименование";
        headersCSV[15] = "Теги";
        headersCSV[16] = "В наличии @Харьков";
        headersCSV[17] = "В наличии @Киев";
        headersCSV[18] = "В наличии";
        putValuesToCSV();
    }

    public void putValuesToCSV() {


        String[] rowData = new String[19];
        Arrays.fill(rowData, "");
        int r;

        dataWriter = new CsvWriter(newCSV_File, "UTF-8", settings());
        dataWriter.writeHeaders();

        //берет артикул из отсортированного по возрастанию массива sortedKeys и извлекает данные из соответсвующему артикулу ряду в XLS
        for (String x : sortedKeys) {
            r = (indexes.get((x))).get(0); //корректоное преобразование из "порядкового номера" в "номер ряда XLS" для чтения данных на прямую из XLS
            HSSFRow currentArticleRow = sheet.getRow(r);
            HSSFCell currentArticleCell;
            Cell priceCellForSell = sheet.getRow(r).getCell(5);

            DecimalFormatSymbols s = new DecimalFormatSymbols();
            s.setDecimalSeparator('.');
            DecimalFormat f = new DecimalFormat("###0.00", s);

            //извлечение данных на прмяую из XLS-CSV  "i" - номер столбца в XLS, "r"-номер ряда
            for (int c = 0; c <= 10; c++) {
                currentArticleCell = currentArticleRow.getCell(c, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
                String value;
                String manufacturer;
                DataFormatter valueFormatter = new DataFormatter();
                value = valueFormatter.formatCellValue(currentArticleCell);
                manufacturer = valueFormatter.formatCellValue(sheet.getRow(r).getCell(2));
                switch (c) {
                    case 0:
                        rowData[0] = value;
                        break;
                    case 1:
                        rowData[1] = value;
                        break;
                    case 2:
                        rowData[3] = value;
                    case 3:
                        String goodName;
                        if (manufacturer.equalsIgnoreCase("КИТАЙ")) {
                            manufacturer = "Standart";
                        }
                        goodName = value + " " + manufacturer + " " + rowData[1] + " " + rowData[0];
                        rowData[14] = goodName;
                        break;
                    case 5:
                        Cell priceCellForBuy = sheet.getRow(r).getCell(5);
                        switch (priceCellForBuy.getCellType()) {
                            case FORMULA:
                                String priceCellForBuyData = String.valueOf(f.format(currentArticleRow.getCell(5).getNumericCellValue()));//получаем цену из листа SKS
                                String result = new DecimalFormat("#0.00").format(CoefficientsForCalcaulations.calculatedPriceBuy(priceCellForBuyData));
                                rowData[5] = result;
                                break;
                            case STRING:
                                if (priceCellForSell.toString().length() > 5) {
                                    priceCellForBuyData = rowData[4].replaceAll(",", ".");//получаем цену из листа SKS
                                    result = new DecimalFormat("#0.00").format(CoefficientsForCalcaulations.calculatedPriceBuy(priceCellForBuyData));
                                    rowData[5] = result;
                                    break;
                                }
                        }
                    case 7:
                        for (int rawDataNum = 7; rawDataNum < 13; rawDataNum++){
                            String tempImgLink = "http://images.sks-service.org/" + rowData[0];
                            rowData[7] = tempImgLink + ".jpg";
                            rowData[rawDataNum] = tempImgLink + "-" + (rawDataNum-7) + ".jpg";
                            }


                        break;
                    case 8:
                        rowData[18] = value;
                        if ( value.equals("нет") ) {
                            rowData[17] = "0";
                        } else {
                            rowData[17] = null;
                        }
                        break;
                }
                //сбор всех значений "подГрупп" при наличии более одной
                if ( indexes.get(x).size() > 1 ) {
                    HSSFCell currentGroupDuplicateleCell;
                    StringJoiner sj = new StringJoiner(",", "{", "}");
                    for (int i = 0; i < (indexes.get((x)).size()); i++) {
                        int z = (indexes.get((x)).get(i));
                        currentGroupDuplicateleCell = sheet.getRow(z).getCell(1, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
                        sj.add(valueFormatter.formatCellValue(currentGroupDuplicateleCell));
                    }
                    String unitegGroups = sj.toString();
                    rowData[1] = unitegGroups;
                }
                //сбор всех значений "Групп" при наличии более одной
                if ( indexes.get(x).size() == 1 ) {
                    int z = (indexes.get((x)).get(0));
                    rowData[2] = valueFormatter.formatCellValue(sheet.getRow(z).getCell(12, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK));
                }
                else if ( indexes.get(x).size() > 1 ) {
                    String rowData2;
                    StringJoiner sj1 = new StringJoiner(",", "{", "}");
                    HSSFCell currentSubGroupDuplicateleCell;
                    for (int i = 0; i < (indexes.get((x)).size()); i++) {
                        int z = (indexes.get((x)).get(i));
                        currentSubGroupDuplicateleCell = sheet.getRow(z).getCell(12, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
                        sj1.add(valueFormatter.formatCellValue(currentSubGroupDuplicateleCell));
                    }
                    rowData2 = sj1.toString();
                    rowData[2] = rowData2;
                }

            }
            String temp = rowData[5];
            Double rowDataFive = CoefficientsForCalcaulations.calculatedPricetoSell(temp);
            rowData[4] = String.valueOf(Math.ceil(rowDataFive)).replaceAll(",", ".");
            //System.out.println(rowData[4]);
            //константа для 7-го столбца
            rowData[6] = "UAH";
            //константа для 11-го столбца
            String tagBuilder = ("Импорт" + dtf.format(localDate));
            rowData[15] = tagBuilder;
            //константа для 12-го столбца
            rowData[16] = "0";
            //константа для 14-го столбца
            rowData[18] = "";
            dataWriter.writeRow(rowData); //запись в CSV по рядам
            System.out.println("Writing " + r + "of " + rows);
        }

        dataWriter.flush();
        System.out.println("~~~~~~~~~DONE~~~~~~~~~");
        dataWriter.close();
        fileToRead.delete();
       // appGui stop = new appGui();
        //stop.stop();

    }
    void Stop(){
        Thread.currentThread().interrupt();
    }

}








