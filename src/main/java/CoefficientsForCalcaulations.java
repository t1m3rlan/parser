import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;

public class CoefficientsForCalcaulations {
    static List<String> operandList;

    public static void setPriceIncome(String priceIncome) {
        CoefficientsForCalcaulations.priceIncome = priceIncome;
    }

    public static String priceIncome;

    public static void setPriceSell(String priceSell) {
        CoefficientsForCalcaulations.priceSell = priceSell;
    }

    public static String priceSell;

    public static Double calculatedPriceBuy(String priceCellForBuyData) {
        double price = Double.parseDouble(priceCellForBuyData.replaceAll("[,]", "."));
        String forBuy = "x*y";


        Expression e = new ExpressionBuilder(forBuy)
                .variables("x", "y")
                .build()
                .setVariable("x", price)
                .setVariable("y", 1 + Double.parseDouble(priceIncome.replaceAll("%",""))/100);
        double result = e.evaluate();
        return result;
    }

    public static Double calculatedPricetoSell(String temp) {
        double price = Double.parseDouble(temp.replaceAll(",", "."));
        String forSell = "x*y+z";

        Expression e = new ExpressionBuilder(forSell)
                .variables("x", "y", "z")
                .build()
                .setVariable("x", price)
                .setVariable("y", 1 + Double.parseDouble(operandList.get(0))/100)
                .setVariable("z", Double.parseDouble(operandList.get(1)));

        double result = e.evaluate();
        return result;
    }



    public static List checkWithRegExp() {

        String formulaResult = priceSell.replaceAll("%","").replaceAll("[a-zA-Zа-яА-Я%()\r\n]", "");
        String[] ready = formulaResult.replaceAll(",", ".").split("[-+*/;]");
        operandList = Arrays.asList(ready);
        return operandList;
    }
}
